/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.m3.uf2;

import java.util.ArrayList;

import ioc.m3.uf2.interficie.SortidaPantalla;
import ioc.m3.uf2.interficie.EntradaTeclat;

import ioc.m3.uf2.models.Pis;
import ioc.m3.uf2.models.Despesa;

import ioc.m3.uf2.controladors.PantallesDespeses;
import ioc.m3.uf2.controladors.PantallesPropietaris;
import ioc.m3.uf2.controladors.PantallesJunta;




/**
 *
 * @author oriol
 * @author victor
 * @author oscar
 */
public class Eac5 {
//    public static final int MAXIM_SALES=10;
//    public static final int MAXIM_AUTORS=50;
//    public static final int MAXIM_OBRES=1000;
    
    SortidaPantalla mostrar = new SortidaPantalla();
    EntradaTeclat lector = new EntradaTeclat();
    PantallesPropietaris pantallesPropietaris = new PantallesPropietaris();
    PantallesDespeses pantallesDespeses = new PantallesDespeses();
    PantallesJunta pantallesJunta = new PantallesJunta();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Eac5 programa = new Eac5();
        
        /*
         * Exemple d'ús de la classe Scanner de ioc.eines per automatitzar el
         * tecleig de dades.
         * Scanner.setTestOn("1\n1\n3\n0\n\n0\n1\n0\n2\n0\n3\n0\n0\ns\n");
         */
        programa.inici();
        /* 
         * Exemple d'ús de la classe Scanner de ioc.eines per finalitzar el
         * tecleig automatitzat de dades.
         * Scanner.setTestOff();
         */        
    }
    
    /** Mètode principal, que resol el menú principal de l'aplicació.
     * 
     */
    public void inici(){

        ArrayList<Pis> comunitat = new ArrayList<Pis>();
        ArrayList<int[]> indexs = new ArrayList<int[]>();
        
        ArrayList<Despesa> facturacio = new ArrayList<Despesa>();

        String[] opcionsTexte = {
            "Gestió de [P]ropietaris", 
            "Gestió de [D]espeses", 
            "Gestió de [C]àrrecs", 
            "[S]ortir",
        };
        char[] opcionsVars = {'P', 'D', 'C', 'S'};
        mostrar.mostraTexte("presentacioPrograma");
        boolean actiu = true;
        while (actiu) {
            System.out.println("MENU PRINCIPAL");
            mostrar.mostraMenu(opcionsTexte);
            char tria = lector.menu(opcionsVars, true);
            switch(tria) {
                case 'P':
                    //mou a "menu propietaris"
                    pantallesPropietaris.menuPropietaris(comunitat, indexs);
                    break;
                case 'D':
                    //mou a "menu despeses"
                    pantallesDespeses.menuDespeses(facturacio, comunitat);
                    break;
                case 'C':
                    //mou a "menu junta"
                    pantallesJunta.menuJunta(comunitat, indexs);
                    break;
                case 'S':
                    //torna al menú principal
                    actiu = false;
                    break;
                default:
                    break;
            }
        }
    }       
}
