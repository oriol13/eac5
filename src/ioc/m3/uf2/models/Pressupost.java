/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.m3.uf2.models;

import ioc.m3.uf2.interficie.EntradaTeclat;
import ioc.m3.uf2.interficie.SortidaPantalla;
import ioc.m3.uf2.interficie.UtilitatsString;
import java.util.ArrayList;

/**
 *
 * @author oriol
 * @author victor
 * @author oscar
 */
public class Pressupost {
    //Instància de classes
    EntradaTeclat lector = new EntradaTeclat();
    SortidaPantalla pantalla = new SortidaPantalla();
    
    /**Mètode per mostrar en forma de cadena de text el pressupost de la comunitat.
     * 
     * @param facturacio conté el pressupost de la comunitat
     * @return cadena de text
     */
    public String mostrarPressupost(ArrayList<Despesa> facturacio){
        System.out.println ("PRESSUPOST DE LA COMUNITAT");
        UtilitatsString.generarString(20, "-");
        String resultat="";
        if (facturacio.isEmpty()) {
            pantalla.mostraTexte("noDespeses");
        } else {
            System.out.println("IMPORT\t\tTIPUS\tDESCRIPCIO");
            for (int i = 0; i<facturacio.size(); i++){
                Despesa despesa = facturacio.get(i);
                int index = i + 1;
                resultat = resultat + index + despesa.mostraDespesa();
            }
        }
        return resultat;       
    }
    
    /**Mètode per calcular l'import total de les despeses que li toca pagar a cada
     * propietari. Segons el tipus de despesa aplica el mètode corresponent.
     * 
     * @param facturacio ArrayList que conté el pressupost de la comunitat
     * @param comunitat ArrayList amb totes les dades dels pisos
     */
    public void calcularImportsPisos(ArrayList<Despesa> facturacio, ArrayList<Pis> comunitat){
        //Declaracio variables
        double[] deutesPisos = new double[comunitat.size()];
    
        //Abans de repartir els imports comprovem si s'ha importat la llista de dades
        //de propietaris.
        //Si hi ha dades a la comunitat...
        if (comunitat.size() > 0) {
            //Fer recorregut per totes les despeses
            for (int i = 0; i < facturacio.size(); i++) {
                //Llegir la despesa
                Despesa despesa = facturacio.get(i);
                double valor = despesa.importDesp;
                switch (despesa.tipus) {
                    case 'A':
                        //Despesa tipus A. Es reparteix el deute per coeficient.
                        despesaA(valor , comunitat, deutesPisos);
                        break;
                    case 'B':
                        //Despesa tipus B. Es reparteix a parts iguals.
                        despesaB(valor, comunitat, deutesPisos);
                        break;
                    case 'C':
                        //Despesa tipus C. Es reparteix entre no exempts.
                        despesaC(valor, comunitat, deutesPisos);
                        break;
                }
            }
            pantalla.mostrarDeutesPerPisos(comunitat, deutesPisos);
        } else {
            //Si no hi ha dades a la comunitat informa l'usuari
            pantalla.mostraTexte("comunitatBuida");
        }
    }
    
    
    /**Recupera la despesa que es vol modificar, copia les dades que han de quedar
     * igual i demana la dada que es vol modificar. Guarda la despesa amb les noves
     * dades i elimina les dades erronies.
     * 
     * @param facturacio ArrayList que conté el pressupost
     * @param numDespesa Enter que indica la posicio de la despesa a modificar
     */
    public void modificarImport(ArrayList<Despesa> facturacio, int numDespesa){
        //Recupera la despesa
        Despesa despesa = facturacio.get(numDespesa);
        //Demana import nou
        double importDesp = lector.llegeixImport();
        //Copia la resta de dades que han de quedar igual
        char tipusDesp = despesa.tipus;
        String descripcioDesp = despesa.descripcio;
        
        //Creem la despesa nova 
        Despesa novaDespesa = new Despesa(importDesp, tipusDesp, descripcioDesp);
        
        //Substituim la despesa al pressupost
        facturacio.set(numDespesa, novaDespesa);
    }
    
    /**Recupera la despesa que es vol modificar, copia les dades que han de quedar
     * igual i demana la dada que es vol modificar. Guarda la despesa amb les noves
     * dades i elimina les dades erronies.
     * 
     * @param facturacio ArrayList que conté el pressupost
     * @param numDespesa Enter que indica la posicio de la despesa a modificar
     */
    public void modificarTipus(ArrayList<Despesa> facturacio, int numDespesa){
        //Recupera la despesa
        Despesa despesa = facturacio.get(numDespesa);
        
        //Demana tipus de despesa nou
        char tipusDesp = lector.llegeixTipusDespesa();
                
        //Copia la resta de dades que han de quedar igual
        double importDesp = despesa.importDesp;
        String descripcioDesp = despesa.descripcio;
        
        //Creem la despesa nova 
        Despesa novaDespesa = new Despesa(importDesp, tipusDesp, descripcioDesp);
        
        //Substituim la despesa al pressupost
        facturacio.set(numDespesa, novaDespesa);
    }
    
    /**Recupera la despesa que es vol modificar, copia les dades que han de quedar
     * igual i demana la dada que es vol modificar. Guarda la despesa amb les noves
     * dades i elimina les dades erronies.
     * 
     * @param facturacio ArrayList que conté el pressupost
     * @param numDespesa Enter que indica la posicio de la despesa a modificar
     */
    public void modificarDescripcio(ArrayList<Despesa> facturacio, int numDespesa){
        //Recupera la despesa
        Despesa despesa = facturacio.get(numDespesa);
        
        //Demana nova descripcio de la despesa
        String descripcioDesp =lector.llegeixDescripcio();
        
        //Copia la resta de dades que han de quedar igual
        double importDesp = despesa.importDesp;
        char tipusDesp = despesa.tipus;
                
        //Creem la despesa nova 
        Despesa novaDespesa = new Despesa(importDesp, tipusDesp, descripcioDesp);
        
        //Substituim la despesa al pressupost
        facturacio.set(numDespesa, novaDespesa);
    }
    
    /**Calcula la part que toca pagar a cada pis si la despesa és de tipus A i 
     * afegeix el resultat als deutes dels pisos
     * 
     * @param valor L'import de la despesa
     * @param comunitat ArrayList amb les dades dels pisos
     * @param deutesPisos Conté els deutes de cada pis 
     */
    public void despesaA(double valor, ArrayList<Pis> comunitat, double[] deutesPisos) {
        //Es reparteix la despesa per coeficient de cada pis
        for (int i = 0; i < comunitat.size(); i++) {
            Pis pis = comunitat.get(i);
            deutesPisos[i] = deutesPisos[i] + (valor*pis.coeficient);
        }
    }
    
    /**Calcula la part que toca pagar a cada pis si la despesa és de tipus B i 
     * afegeix el resultat als deutes dels pisos
     * 
     * @param valor L'import de la despesa
     * @param comunitat ArrayList amb les dades dels pisos
     * @param deutesPisos Conté els deutes de cada pis 
     */
    public void despesaB(double valor, ArrayList<Pis> comunitat, double[] deutesPisos) {
        //Es reparteix la despesa a parts iguals entre tots els propietaris
        for (int i = 0; i<comunitat.size(); i++) {
            deutesPisos[i] = deutesPisos[i] + (valor/comunitat.size());
        }
    }
    
     /**Calcula la part que toca pagar a cada pis si la despesa és de tipus C i 
     * afegeix el resultat als deutes dels pisos
     * 
     * @param valor L'import de la despesa
     * @param comunitat ArrayList amb les dades dels pisos
     * @param deutesPisos Conté els deutes de cada pis 
     */
    public void despesaC(double valor, ArrayList<Pis> comunitat, double[] deutesPisos) {
        //Calcula el numero de no exempts per repartir la despesa
        int noExempts = comptarNoExempts(comunitat);
        //Calcula la part de despesa que toca a cada pis
        double valorPerPis = valor / noExempts;
        //Fem el recorregut per l'ArrayList de la comunitat
        for (int i = 0; i<comunitat.size(); i++ ) {
            //Recuperem el pis de la posició corresponent
            Pis pis = comunitat.get(i);
            //Si el pis és NO EXEMPT li apliquem la part que li pertoca de despesa
            if (!pis.exempt) {
                deutesPisos[i] += valorPerPis;
            }
        }
    }
    
    /**Mètode que compta el número de pisos no exempts de pagar les despeses de tipus
     * C.
     * 
     * @param comunitat ArrayList que conté les dades de la comunitat
     * @return Enter amb el número de pisos no exempts
     */
    public int comptarNoExempts(ArrayList<Pis> comunitat){
        int noExempts=0;
        //Fem el recorregut per l'ArrayList de la comunitat
        for (int i = 0; i<comunitat.size(); i++ ) {
            //Recuperem el pis de la posició corresponent
            Pis pis = comunitat.get(i);
            //Si el pis és NO EXEMPT incrementem el comptador
            if (!pis.exempt) {
                noExempts++;
            }
        }    
        return noExempts;
    }
}