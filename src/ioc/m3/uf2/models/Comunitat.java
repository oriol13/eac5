/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.m3.uf2.models;

import java.util.ArrayList;
import ioc.eines.Gestoria;

/**
 *
 * @author oriol
 * @author victor
 * @author oscar
 */
public class Comunitat {
    
    static final int NATURAL = 0;
    static final int ALFABETIC = 1;
    static final int COEF = 2;
    
    Gestoria gestoria = new Gestoria();
    
    /** Mètode que usem per importar les dades de la gestoria i convertir-les a
     * dades usables pel nostre sistema.
     * 
     * @param comunitat ArrayList de pisos.
     * @param indexs ArrayList d'indexs.
     */
    public void importarDades(ArrayList<Pis> comunitat, ArrayList<int[]> indexs) {
        //Importo les dades i les tracto per crear els objectes.
        String cadena = gestoria.importarDades();
        String[] registres = cadena.split("#");
        
        //Creo els indexs i els afegeixo a l'arraylist.
        int[] indx = new int[Integer.parseInt(registres[0].trim())];
        int[] indxNom = new int[Integer.parseInt(registres[0].trim())];
        int[] indxCoef = new int[Integer.parseInt(registres[0].trim())];
        indexs.add(NATURAL, indx);
        indexs.add(ALFABETIC, indxNom);
        indexs.add(COEF, indxCoef);
        
        //Itero en els registres creant un objecte Pis de cada registre i l'indexo.
        for (int i = 1; i < registres.length; i++) {
            String[] camps = registres[i].split(";");
            String id = camps[0].trim();
            String nom = camps[1].trim();
            String propietari = camps[2].trim();
            String tel = camps[3].trim();
            boolean resident = false;
            if (camps[4].trim().charAt(0) == 'S') {
                resident = true;
            }
            double coeficient = Double.parseDouble(camps[5].trim());
            boolean exempt = false;
            if (Integer.parseInt(camps[6].trim()) == 1) {
                exempt = true;
            }
            char junta = 'x';
            if (camps.length > 7) {
                junta = camps[7].trim().charAt(0);
            }
            Pis pis = new Pis(id, nom, propietari, tel, resident, coeficient, exempt, junta);
            comunitat.add(pis);
            
            int k = i - 1;
            //indexo a l'index natural.
            indx[k] = k;
            
            //indexo a l'index alfabètic.
            indexaNom(k, comunitat, indexs);
            
            //indexo a l'index per coeficients.
            indexaCoef(k, comunitat, indexs);
        }

    }
    
    /** Mètode que ens retorna una cadena de text que ens mostra la comunitat,
     * en funció d'un dels índexs que disposem.
     * 
     * @param comunitat ArrayList amb els pisos de la comunitat.
     * @param indexs ArrayList amb els índexs.
     * @param index índex amb el que mostrarem la comunitat.
     * @return una cadena de text amb la comunitat correctament formatada.
     */
    public String mostrarComunitat(ArrayList<Pis> comunitat, ArrayList<int[]> indexs, int index) {
        String resultat = "LLISTA DE PROPIETARIS ";
        switch (index) {
            case NATURAL:
                break;
            case ALFABETIC:
                resultat += "ORDENADA PER NOM";
                break;
            case COEF:
                resultat += "ORDENADA PER COEFICIENTS";
                break;
        }
        resultat += "\n-----------------------------------------------------\n";
        for (int i = 0; i < comunitat.size(); i++) {
            Pis pis = comunitat.get(indexs.get(index)[i]);
            resultat += pis.mostraPis();
        }
        return resultat;
    }
    
    /** Mètode que ens indexa un nou pis de la comunitat en funció del nom del 
     * propietari.
     * 
     * @param i posició del pis que afegim.
     * @param comunitat ArrayList amb els pisos de la comunitat.
     * @param indexs ArrayList amb els índexs.
     */
    private void indexaNom(int i, ArrayList<Pis> comunitat, ArrayList<int[]> indexs) {
        int[] indxNom = indexs.get(ALFABETIC);
        indxNom[i] = i;
        for (int j = i - 1; j >= 0; j--) {
            if (comunitat.get(indxNom[j]).propietari.compareToIgnoreCase(comunitat.get(i).propietari) > 0) {
                indxNom[j + 1] = indxNom[j];
                indxNom[j] = i;
            }
        }
    }
    
    /**Mètode que ens indexa un nou pis de la comunitat en funció del 
     * coeficient del pis. 
     * 
     * @param i posició del pis que afegim.
     * @param comunitat ArrayList amb els pisos de la comunitat.
     * @param indexs ArrayList amb els índexs.
     */
    private void indexaCoef(int i, ArrayList<Pis> comunitat, ArrayList<int[]> indexs) {
        int[] indxCoef = indexs.get(COEF);
        indxCoef[i] = i;
        for (int j = i - 1; j >= 0; j--) {
            if (comunitat.get(indxCoef[j]).coeficient > comunitat.get(i).coeficient) {
                indxCoef[j + 1] = indxCoef[j];
                indxCoef[j] = i;
            }
        }        
    }
    
    /** Mètode que ens retorna una llista de strings amb els identificadors dels
     * pisos.
     * 
     * @param comunitat ArrayList amb els pisos de la comunitat.
     * @param indexs ArrayList amb els índexs.
     * @return Array d'identificadors dels pisos.
     */
    public String[] identificadors(ArrayList<Pis> comunitat, ArrayList<int[]> indexs) {
        String[] resultat = new String[comunitat.size()];
        for (int i = 0; i < comunitat.size(); i++) {
            Pis pis = comunitat.get(indexs.get(NATURAL)[i]);
            resultat[i] = pis.id;
        }
        return resultat;
    }
    
    /** Mètode que ens retorna un string amb la llista de pisos correctament 
     * formatada per poder seleccionar un dels pisos. 
     * 
     * @param comunitat ArrayList amb els pisos de la comunitat.
     * @param indexs ArrayList amb els índexs.
     * @return llista de pisos formatada per poder-ne seleccionar un.
     */
    public String mostraComunitatSeleccionar(ArrayList<Pis> comunitat, ArrayList<int[]> indexs) {
        String resultat = "\nCODI. Dades del pis." +
         "\n-----------------------------------------------------\n";
        for (int i = 0; i < comunitat.size(); i++) {
            Pis pis = comunitat.get(indexs.get(NATURAL)[i]);
            resultat += pis.mostraPisSeleccionar();
        }
        return resultat;
    }
    
    /** Mètode per reindexar un pis quan el nom del propietari s'ha modificat.
     * 
     * @param i posició del pis que afegim.
     * @param comunitat ArrayList amb els pisos de la comunitat.
     * @param indexs ArrayList amb els índexs.
     */
    public void reIndexaNom(int i, ArrayList<Pis> comunitat, ArrayList<int[]> indexs) {
        int[] indxNom = indexs.get(ALFABETIC);
        //Primer busquem la posició que tenia abans de canviar el nom i movem 
        //tots els registres superiors.
        for (int j = 0; j < indxNom.length - 1; j++) {
            boolean trobat = false;
            if (indxNom[j] == i) {
                trobat = true;
            }
            if (trobat) {
                indxNom[j] = indxNom[j + 1];
            }
        }
        
        //Indexem el nou valor.
        indxNom[indxNom.length - 1] = i;
        for (int j = indxNom.length - 2; j >= 0; j--) {
            if (comunitat.get(indxNom[j]).propietari.compareToIgnoreCase(comunitat.get(i).propietari) > 0) {
                indxNom[j + 1] = indxNom[j];
                indxNom[j] = i;
            }
        }
    }
    
    /** Mètode per reindexar un pis quan el coeficient del pis s'ha modificat.
     * 
     * @param i posició del pis que afegim.
     * @param comunitat ArrayList amb els pisos de la comunitat.
     * @param indexs ArrayList amb els índexs.
     */
    public void reIndexaCoef(int i, ArrayList<Pis> comunitat, ArrayList<int[]> indexs) {
        int[] indxCoef = indexs.get(COEF);
        //Primer busquem la posició que tenia abans de canviar el nom i movem 
        //tots els registres superiors.
        for (int j = 0; j < indxCoef.length - 1; j++) {
            boolean trobat = false;
            if (indxCoef[j] == i) {
                trobat = true;
            }
            if (trobat) {
                indxCoef[j] = indxCoef[j + 1];
            }
        }
        
        //Indexem el nou valor.
        indxCoef[indxCoef.length - 1] = i;
        for (int j = indxCoef.length - 2; j >= 0; j--) {
            if (comunitat.get(indxCoef[j]).coeficient > comunitat.get(i).coeficient) {
                indxCoef[j + 1] = indxCoef[j];
                indxCoef[j] = i;
            }
        }
    }
}
