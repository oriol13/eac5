/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.m3.uf2.models;

/**
 *
 * @author oriol
 * @author victor
 * @author oscar
 */
public class Pis {
    public String id = "";
    public String nom = "";
    public String propietari = "";
    public String tel = "";
    public boolean resident = false;
    public double coeficient = 0.0;
    public boolean exempt = false;
    public char junta = 0;
    
    /** Objecte pis.
     * 
     * @param pisId Identificador del pis.
     * @param pisNom Text amb el que es coneix el pis.
     * @param propNom Nom del propietari.
     * @param propTel Telèfon del propietari.
     * @param propResident Indica si el propietari viu al pis.
     * @param pisCoef Coeficient que correspon al pis.
     * @param propExempt Indica si el propietari està exempt de les despeses de tipus C.
     * @param carrecJunta Indica quin és el càrrec a la junta, pot ser 'P', 'V' i 'S'.
     */
    public Pis(
            String pisId, 
            String pisNom, 
            String propNom,
            String propTel,
            boolean propResident,
            double pisCoef,
            boolean propExempt,
            char carrecJunta
    ) {
        id = pisId;
        nom = pisNom;
        propietari = propNom;
        tel = propTel;
        resident = propResident;
        coeficient = pisCoef;
        exempt = propExempt;
        junta = carrecJunta;
    }
    
    /** Mètode que retorna les dades del pis en forma de cadena de texte,
     * formatat tal com es demana als llistats de pisos.
     * 
     * @return les dades del pis formatades.
     */
    public String mostraPis() {
        String resultat = "";
        resultat += "- " + nom + " (" + coeficient + "): " +
                propietari + " (" + tel + ")";
        if (!resident) {
            resultat += " -Absent-";
        }
        resultat +="\n";
        return resultat;
    }
    
    /** Mètode que retorna les dades del pis en forma de cadena de texte,
     * formatat per seleccionar el pis a la pantalla de modificar dades del pis.
     * 
     * @return les dades del pis formatades per la selecció.
     */
    public String mostraPisSeleccionar() {
        String resultat = id + "    - " + nom + ": " + propietari + " (" + tel + ")";
        if (!resident) {
            resultat += " -Absent-";
        }
        resultat += "\n";
        return resultat;
    }
    
    /** Mètode que retorna les dades del pis per modificar formatades de manera
     * que quedin numerades per poder-les seleccionar.
     * 
     * @return les dades del pis formatades per poder-les seleccionar.
     */
    public String mostraPropietari() {
        String resultat = "\nPis: " + nom;
        resultat += "\n     1) Nom propietari: " + propietari; 
        resultat += "\n     2) Telèfon: " + tel; 
        resultat += "\n     3) Coeficient: " + coeficient; 
        resultat += "\n     4) Exempt de despeses C: ";
        if (exempt) {
            resultat += "si";
        } else {
            resultat += "no";
        }
        resultat += "\n     5) El propietari està absent: "; 
        if (resident) {
            resultat += "si";
        } else {
            resultat += "no";
        }
        return resultat;
    }
}
