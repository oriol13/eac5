/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.m3.uf2.models;

import java.util.ArrayList;

/**
 *
 * @author oscar
 * @author oriol
 * @author victor
 */
public class Junta {
    
       
    /** Mètode per seleccionar el President, vicepresicent i secretari
     * 
     * @param comunitat
     * @return 
     */
    public String[] mostrarJuntaActual(ArrayList<Pis> comunitat) {
        
        String[] carrecsJunta=new String[3];
        for (int i = 0; i < comunitat.size(); i++) {
        Pis pis = comunitat.get(i);
            
            if(pis.junta=='P'){
                
                carrecsJunta[0]="" + pis.propietari + ","
                    + " telf " + pis.tel +
                    "("+pis.nom+")"; 
            }
            else if(pis.junta=='V'){
                
                carrecsJunta[1]=""+ pis.propietari + ","
                    + " telf " + pis.tel +
                    "("+pis.nom+")"; 
            }
            else if(pis.junta=='S'){
                
                carrecsJunta[2]=""+ pis.propietari + ","
                    + " telf " + pis.tel +
                    "("+pis.nom+")"; 
            }
        }
        return carrecsJunta;
    }
    
    
    /** Mètode que reseteja les dades de la junta.
     * 
     * @param comunitat ArrayList amb els pisos de la comunitat.
     */
    public void resetDades(ArrayList<Pis> comunitat){
        for (int i = 0; i < comunitat.size(); i++) {
            Pis pis = comunitat.get(i);
            pis.junta='x';
            comunitat.set(i,pis);
        }
    }
    
    
    /** modifica pis.junta i introdueix el president a la seguent posicio,
     * sempre que siguin residents a partir de la posicio
     * 
     * @param comunitat ArrayList amb els pisos de la comunitat.
     * @param posicio index del pis
     */
    public void modificarPresident(ArrayList<Pis> comunitat, int posicio){
        int cont=0;
        do{
            cont++;
            Pis pis = comunitat.get(posicio);
            posicio++;
               if(pis.resident){
                    if (pis.junta=='x'){
                        pis.junta='P';
                        comunitat.set(posicio,pis);
                        cont=comunitat.size()+1;
                    }
                }
            if(posicio==comunitat.size()){
                posicio=0;
            }  
        }while(cont<=comunitat.size());
    }
    
    /** modifica pis.junta i introdueix el vicepresident a la seguent posicio, 
     * sempre que siguin residents a partir de la posicio
     * 
     * @param comunitat ArrayList amb els pisos de la comunitat.
     * @param posicio index del pis
     */
    public void modificarVicepresident(ArrayList<Pis> comunitat, int posicio){
        int cont=0;
        do{
           cont++;
           Pis pis = comunitat.get(posicio);
           posicio++;
                if(pis.resident){
                    if (pis.junta=='x'){
                        pis.junta='V';
                        comunitat.set(posicio,pis);
                        cont=comunitat.size()+1;
                    }
                    
                }
                if(posicio==comunitat.size()){
                    posicio=0;
                }  
         }while(cont<=comunitat.size());
    }
    
    /** modifica pis.junta i introdueix el secretari a la seguent posicio, 
     * sempre que siguin residents a partir de la posicio
     * 
     * @param comunitat ArrayList amb els pisos de la comunitat.
     * @param posicio index del pis
     */
    public void modificarSecretari(ArrayList<Pis> comunitat, int posicio){
     
        int cont=0;
        do{
            cont++;
            Pis pis = comunitat.get(posicio);
            posicio++;
               if(pis.resident){
                    if (pis.junta=='x'){
                        pis.junta='S';
                        comunitat.set(posicio,pis);
                        cont=comunitat.size()+1;
                    }
                }
            if(posicio==comunitat.size()){
                 posicio=0;
             }  
            
         }while(cont <= comunitat.size());
        
    }
    
    /** torna la posició en la qual hem de iniciar la modificació de la 
     * junta o propietari
     * 
     * @param comunitat ArrayList amb els pisos de la comunitat.
     * @return posició a partir de la qual modificar la junta
     */
    public int tornarposicio(ArrayList<Pis> comunitat){
        int posicio=0;
        
        for (int i = 0; i < comunitat.size(); i++) {
            Pis pis = comunitat.get(i);
                
                if(pis.junta=='S'){
                posicio=i;
                posicio=posicio+1;
                }
            }
        return posicio;      
    }
      
   
    /** Mou els carrecs per modificar el president de la junta
     * 
     * @param comunitat ArrayList amb els pisos de la comunitat.
     */
    public void moureCarrecsPresident(ArrayList<Pis> comunitat){
      
        for (int i = 0; i < comunitat.size(); i++) {
            
            Pis pis = comunitat.get(i);
            if(pis.junta=='P'){
                pis.junta='x';
            }
            if(pis.junta=='V'){
                pis.junta='P';
            }
            if(pis.junta=='S'){
                pis.junta='V';
            }
        comunitat.set(i,pis);
        }    
    }
    
    
    /** Mou els carrecs per modificar el vicepresident de la junta
     * 
     * @param comunitat ArrayList amb els pisos de la comunitat.
     */
    public void moureCarrecsVicepresident(ArrayList<Pis> comunitat){
         
         for (int i = 0; i < comunitat.size(); i++) {
         Pis pis = comunitat.get(i);
              
             if(pis.junta=='V'){
                 pis.junta='x';
             }
             
             if(pis.junta=='S'){
                pis.junta='V';
             }
             comunitat.set(i,pis);
        }  
    }
    
    
    /** Mou els carrecs per modificar el secretari de la junta
     * 
     * @param comunitat ArrayList amb els pisos de la comunitat.
     */
    public void moureCarrecsSecretari(ArrayList<Pis> comunitat){
         
        for (int i = 0; i < comunitat.size(); i++) {
        Pis pis = comunitat.get(i);
                    
                    if(pis.junta=='S'){
                        pis.junta='x';
                    }
                    comunitat.set(i,pis);
        }  
    }
}