/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.m3.uf2.models;
import ioc.m3.uf2.interficie.EntradaTeclat;
import ioc.m3.uf2.interficie.SortidaPantalla;
import java.util.Scanner;

/**
 *
 * @author oriol
 * @author victor
 * @author oscar
 */
public class Despesa {
    
    Scanner scanner = new Scanner(System.in);
    EntradaTeclat lector = new EntradaTeclat();
    SortidaPantalla pantalla = new SortidaPantalla();
    
    public double importDesp = 0;
    public char tipus = 0;
    public String descripcio = "";
        
    /** Objecte Pressupost.
     * 
     * @param despImport Import de la despesa.
     * @param despTipus Tipus de la despesa. Pot ser 'A', 'B' o 'C'.
     * @param despDescripcio Descripció de la despesa.
     */
    public Despesa(
            double despImport,
            char despTipus,
            String despDescripcio
    ) {
        importDesp = despImport;
        tipus = despTipus;
        descripcio = despDescripcio;
    }
    
    /**Metode que retorna la despesa com a cadena de text.
     * 
     * @return Cadena de text
     */
    public String mostraDespesa() {
        String resultat = "";
        resultat += "- " + importDesp + " \t " + tipus + " \t " +
                descripcio;
        
        resultat +="\n";
        return resultat;
    }
}