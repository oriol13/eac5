/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.m3.uf2.controladors;

import java.util.ArrayList;
import ioc.m3.uf2.interficie.SortidaPantalla;
import ioc.m3.uf2.models.Comunitat;
import ioc.m3.uf2.interficie.EntradaTeclat;
import ioc.m3.uf2.models.Pis;

/**
 *
 * @author oriol
 * @author victor
 * @author oscar
 */
public class PantallesPropietaris {
    SortidaPantalla mostrar = new SortidaPantalla();
    EntradaTeclat lector = new EntradaTeclat();
    Comunitat comunitatUtils = new Comunitat();
    
    /** Mètode que mostra la presentació de la secció "propietaris", i demana
     * que es trii una opció de del menú propietaris i resol l'opció triada.
     * 
     * @param comunitat Arraylist de pisos
     * @param indexs Arraylist d'indexs
     */
    public void menuPropietaris(ArrayList<Pis> comunitat, ArrayList<int[]> indexs) {
//        SortidaPantalla mostrar = new SortidaPantalla();
//        EntradaTeclat lector = new EntradaTeclat();
        String[] opcionsTexte = {
            "[I]mportar propietaris",
            "[M]odificar propietaris",
            "[C]onsulta propietaris",
            "[T]ornar"
        };
        char[] opcionsVars = {'I', 'M', 'C', 'T'};
        mostrar.mostraTexte("presentacioPropietaris");
        boolean triat = false;
        while (!triat) {
            mostrar.mostraMenu(opcionsTexte);
            char tria = lector.menu(opcionsVars, true);
            switch(tria) {
                case 'I':
                    //mou a "importar propietaris"
                    importaPropietaris(comunitat, indexs);
                    break;
                case 'M':
                    //mou a "modificar propietaris"
                    seleccionaPisModificar(comunitat, indexs);
                    
                    break;
                case 'C':
                    //mou a "consultar propietaris"
                    consultaPropietaris(comunitat, indexs);
                    break;
                case 'T':
                    //torna al menú principal
                    triat = true;
                    break;
                default:
                    break;
            }
        }
        
    }
    
    /** Mètode per resoldre la importació de les dades
     * 
     * @param comunitat ArrayList de pisos
     * @param indexs ArrayList d'indexs
     */
    public void importaPropietaris(ArrayList<Pis> comunitat, ArrayList<int[]> indexs) {
        comunitatUtils.importarDades(comunitat, indexs);
        String comunitatStr = comunitatUtils.mostrarComunitat(comunitat, indexs, 0);
        mostrar.mostraString(comunitatStr);
    }
    
    /** Mètode per resoldre la modificació de les dades d'un dels propietaris.
     * Requereix com arguments l'array de propietaris i l'array d'indexs.
     * 
     * @param comunitat ArrayList de pisos
     * @param indexs ArrayList d'indexs
     */
    public void seleccionaPisModificar(ArrayList<Pis> comunitat, ArrayList<int[]> indexs) {
        if (comunitat.size() > 0) {
            String[] opcions = comunitatUtils.identificadors(comunitat, indexs);
            String comunitatStr = comunitatUtils.mostraComunitatSeleccionar(comunitat, indexs);
            boolean fet = false;
            while (!fet) {
                mostrar.mostraString(comunitatStr);
                mostrar.mostraTexte("seleccionaPis");
                String tria = lector.seleccionaPisModificar(opcions);
                switch(tria) {
                    case "x":
                        break;
                    case "C":
                        fet = true;
                        break;
                    default:
                        modificarPis(comunitat, indexs, tria);
                        break;
                }
            }
        } else {
            mostrar.mostraTexte("comunitatBuida");
        }
    }
    
    /** Mètode per resoldre la modificació de les dades d'un dels propietaris.
     * Requereix com arguments l'array de propietaris, l'array d'indexs i la
     * posició que es vol modificar.
     * 
     * @param comunitat ArrayList de pisos.
     * @param indexs ArrayList d'indexs.
     * @param pisId id del pis que es vol modificar.
     */
    public void modificarPis(ArrayList<Pis> comunitat, ArrayList<int[]> indexs, String pisId) {
        int pisIndx = -1;
        for (int i = 0; i < comunitat.size(); i++) {
            Pis pis = comunitat.get(i);
            if (pis.id == pisId) {
                pisIndx = i;
            }
        }
        Pis pis = comunitat.get(pisIndx);
        char[] opcionsVars = {'1', '2', '3', '4', '5', 'C'};
        boolean fet = false;
        while (!fet) {
            mostrar.mostraString(pis.mostraPropietari());
            mostrar.mostraTexte("seleccionaDadaModificar");
            char tria = lector.menu(opcionsVars, true);
            switch(tria) {
                case '1':
                    //modifica el nom del propietari.
                    mostrar.mostraTexte("demanaNom");
                    pis.propietari = lector.llegeixString();
                    comunitatUtils.reIndexaNom(pisIndx, comunitat, indexs);
                    comunitat.set(pisIndx, pis);
                    break;
                case '2':
                    //modifica el telèfon.
                    mostrar.mostraTexte("demanaTel");
                    pis.tel = lector.llegeixString();
                    comunitat.set(pisIndx, pis);
                    break;
                case '3':
                    //modifica el coeficient.
                    mostrar.mostraTexte("demanaCoef");
                    pis.coeficient = lector.llegeixReal();
                    comunitatUtils.reIndexaCoef(pisIndx, comunitat, indexs);
                    comunitat.set(pisIndx, pis);
                    break;
                case '4':
                    //modifica l'exemció de despeses de tipus C.
                    mostrar.mostraTexte("demanaExempt");
                    pis.exempt = lector.llegeixBool();
                    comunitat.set(pisIndx, pis);
                    break;
                case '5':
                    //modifica si el propietari està absent.
                    mostrar.mostraTexte("demanaAbsent");
                    pis.resident = lector.llegeixBool();
                    comunitat.set(pisIndx, pis);
                    break;
                case 'C':
                    //Surt.
                    fet = true;
                    break;
                default:
                    break;
            }
        }
    }
    
    /** Mètode per seleccionar l'index des del que visualitzar els propietaris i
     * mostrar-los.
     * 
     * @param comunitat ArrayList de pisos.
     * @param indexs ArrayList d'indexs.
     */
    public void consultaPropietaris(ArrayList<Pis> comunitat, ArrayList<int[]> indexs) {
        mostrar.mostraTexte("demanaIndexMostrarCom");
        char[] opcions = {'A', 'C'};
        char tria = lector.menu(opcions, false);
        int indx = 0;
        switch(tria) {
            case 'A':
                indx = 1;
                break;
            case 'C':
                indx = 2;
                break;
        }
        String resultat = comunitatUtils.mostrarComunitat(comunitat, indexs, indx);
        mostrar.mostraString(resultat);
    }
}
