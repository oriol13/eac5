/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.m3.uf2.controladors;

import java.util.ArrayList;
import ioc.m3.uf2.interficie.SortidaPantalla;
import ioc.m3.uf2.models.*;
import ioc.m3.uf2.interficie.EntradaTeclat;
import ioc.m3.uf2.models.Pis;

      

/**
 *
 * @author oriol
 * @author victor
 * @author oscar
 */
public class PantallesJunta {
     
    
    SortidaPantalla mostrar = new SortidaPantalla();
    EntradaTeclat lector = new EntradaTeclat();
    Comunitat comunitatUtils = new Comunitat();
    Junta juntaUtils = new Junta();



    
    
    /**
      *Metode que mostra el menu despeses i demana que es trii 
      * una opciÃ³ i posteriorment resol la opciÃ³ triada.
      *
      */
    public void menuJunta(ArrayList<Pis> comunitat, ArrayList<int[]> indexs) {
        comunitatUtils.importarDades(comunitat, indexs);
        String[] opcionsTexte = {
          "[A]: Mostrar Junta",
          "[R]: Resol els nous càrrecs (renovar càrrecs) i mostra 'Mostrar junta'.",
          "[S]: Resol els nous càrrecs (substituir càrrec) i mostra 'Mostrar junta'",
          "[T]: Menu Principal"
          
        };
        char[] opcionsVars = {'A', 'R', 'S','T'};
        
        boolean triat = false;
        while (!triat) {
            mostrar.mostraMenu(opcionsTexte);
            char tria = lector.menu(opcionsVars, true);
            switch(tria) {
                case 'A':
                    //mou a "mostrarJunta"
                    mostrarjunta(comunitat);
                    
                    break;
                case 'R':
                    //mou a "RenovarCarrecs"
                    renovarCarrecs(comunitat);
                    break;
                case 'S':
                    //mou a "SubstituirCarrec"
                    substituirCarrec(comunitat);
                    break;
                case 'T':
                    //torna al menu principal
                    triat=true;
                    break;
                default:
                    break;
            }
    }
} 
    
    /** Metode que mostra la junta Actual
     * 
     * @param comunitat 
     */
    public void mostrarjunta(ArrayList<Pis> comunitat){
       
        
        
        String[] MostrarJunta = juntaUtils.mostrarJuntaActual(comunitat);
        mostrar.mostraJunta(MostrarJunta);
        
    }

    /** Metode que s'encarrega de renovar els carrecs i mostrar la renovaciÃ³
     * 
     * @param comunitat 
     */
    public void renovarCarrecs(ArrayList<Pis> comunitat){
        
        
        int posInici=juntaUtils.tornarposicio(comunitat);
        juntaUtils.resetDades(comunitat);
        juntaUtils.modificarPresident(comunitat, posInici);
        juntaUtils.modificarVicepresident(comunitat,posInici);
        juntaUtils.modificarSecretari(comunitat,posInici);
        String[] MostrarJunta = juntaUtils.mostrarJuntaActual(comunitat);
        mostrar.mostraJunta(MostrarJunta);
   
    }
    
    
    
    /**
     * Mostrar el menu substituirCarrec:
     * 
     * Tenim 3 opcions:
     * Modificar el carrec President, Vicepresident, Secretari o Tornar Menu principal
     * -En cas de Substituir el President, haurem de pasar vice a presi i secre a vicepre
     * -En cas de Substituir el Vicepresident, haurem de pasar Secre a Vice y subs
     * tituir secretari.
     * -En cas de Substituir el Secretari, haurem de substituir el secretari.
     * 
     * @param substituirCarrec
     * @param indexs 
     */
    
    public void substituirCarrec(ArrayList<Pis> comunitat){
        
        
        String[] opcionsTexte = {"Quin Carrec Vols Substituir?",
          "[P]: President",
          "[V]: Vicepresident",
          "[S]: Secrectari",
          "[T]: Menu Principal"
          
        };
        char[] opcionsVars = {'P', 'V', 'S','T'};
       
        boolean triat = false;
        while (!triat) {
            mostrar.mostraMenu(opcionsTexte);
            
            int posInici=juntaUtils.tornarposicio(comunitat);
            char tria = lector.menu(opcionsVars, true);
            switch(tria) {
                
                case 'P':
                    //Canvi els carrecs i busca un secretari 
                    renovarPresident(comunitat);
                    break;
                case 'V':
                    //Canvia els carrecs i busca un secretari
                    renovarVicepresident(comunitat);
                    break;
                case 'S':
                    //Busca un secretari
                  
                    renovarSecretari(comunitat);
                    break;
                case 'T':
                    
                    //torna al menu principal
                    triat=true;
                    break;
                default:
                    break;
            }
        }
    }
    
    /**
     * 
     * @param comunitat 
     */
    public void renovarPresident(ArrayList<Pis> comunitat){
                   
                    juntaUtils.moureCarrecsPresident(comunitat);
                    int posInici=juntaUtils.tornarposicio(comunitat);
                    juntaUtils.modificarSecretari(comunitat,posInici);
                    String[] MostrarJunta = juntaUtils.mostrarJuntaActual(comunitat);
                    mostrar.mostraJunta(MostrarJunta);
    }
    
    /**
     * 
     * @param comunitat 
     */
    public void renovarVicepresident(ArrayList<Pis> comunitat){
                    
                    juntaUtils.moureCarrecsVicepresident(comunitat);
                    int posInici=juntaUtils.tornarposicio(comunitat);
                    juntaUtils.modificarSecretari(comunitat,posInici);
                    String[] MostrarJunta = juntaUtils.mostrarJuntaActual(comunitat);
                    mostrar.mostraJunta(MostrarJunta);
    }
    
    /**
     * 
     * @param comunitat 
     */
    public void renovarSecretari(ArrayList<Pis> comunitat){
                    
                    juntaUtils.moureCarrecsSecretari(comunitat);
                    int posInici=juntaUtils.tornarposicio(comunitat);
                    juntaUtils.modificarSecretari(comunitat,posInici);
                    String[] MostrarJunta = juntaUtils.mostrarJuntaActual(comunitat);
                    mostrar.mostraJunta(MostrarJunta);
    }
    
}
