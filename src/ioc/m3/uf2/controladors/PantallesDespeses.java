/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.m3.uf2.controladors;

import java.util.ArrayList;

import ioc.m3.uf2.interficie.UtilitatsString;
import ioc.m3.uf2.interficie.SortidaPantalla;
import ioc.m3.uf2.interficie.EntradaTeclat;

import ioc.m3.uf2.models.Pis;
import ioc.m3.uf2.models.Pressupost;
import ioc.m3.uf2.models.Despesa;

/**Mostra els menus de despeses i en tracta les opcions escollides
 *
 * @author oriol
 * @author victor
 * @author oscar
 */
public class PantallesDespeses {

    SortidaPantalla mostrar = new SortidaPantalla();
    EntradaTeclat lector = new EntradaTeclat();
    Pressupost pressupost = new Pressupost();

    /**Mostra el menú Despeses i crida el mètode corresponent a l'opció escollida
     * fins que aquesta sigui sortir.
     * 
     * @param facturacio ArrayList Conté totes les despeses
     * @param comunitat ArrayList Conté les dades de la comunitat
     * @author victor
     */
    public void menuDespeses (ArrayList<Despesa> facturacio, ArrayList<Pis> comunitat){
        //Declaracio variables.
        String[] menu ={"[M]ostrar pressupost",
                        "[E]ditar pressupost",
                        "[C]alcular imports de cada pis",
                        "[H] Ajuda",
                        "[T]ornar"};
        char[] opcions={'M','E','C','H','T'};
        
        //Es mostra el menú i es tracten respostes fins que es seleccioni sortir.
        boolean sortir = false;
        char opcio;
        while (!sortir){
            System.out.println("MENU DESPESES");
            System.out.println(UtilitatsString.generarString(20,"-"));
            System.out.println("menu principal > despeses");
            mostrar.mostraMenu(menu);
            opcio = lector.menu(opcions,true);
            switch (opcio){
                case 'M':
                    //Mostra pressupost
                    System.out.println(pressupost.mostrarPressupost(facturacio));
                    break;
                case 'E':
                    //Mostra menu editar pressupost
                    menuEdicioDesp(facturacio);
                    break;
                case 'C':
                    //Calcula imports de cada pis
                    pressupost.calcularImportsPisos(facturacio, comunitat);
                    break;
                case 'H':
                    //Mostra ajuda del menu
                    mostrar.mostraTexte("ajudaDespeses");
                    break;
                case 'T':
                    //Surt del menú despeses
                    sortir=true;
                    break;
                default:
                    break;
            }
        }
    }
    
    /**Mostra el menú per editar les despeses i en tracta les opcions fins que es
     * selecciona sortir.
     * 
     * @param facturacio Conté totes les despeses de la comunitat
     * @author victor
     */
    public void menuEdicioDesp(ArrayList<Despesa> facturacio) {
        //Declaració de variables
        String[] menu = {"[A]fegir despesa",
                         "[M]odificar despesa",
                         "[E]liminar despesa",
                         "[H] Ajuda",
                         "[T]ornar"};
        char[] opcions ={'A','M','E','H','T'};
                
        //Es mostra el menú i es tracten respostes fins que es seleccioni sortir.
        boolean sortir = false;
        char opcio;
        while (!sortir){
            System.out.println("MENU D'EDICIÓ DE LES DESPESES");
            System.out.println(UtilitatsString.generarString(30,"-"));
            System.out.println("menu principal > despeses > edició de despeses");
            mostrar.mostraMenu(menu);
            opcio = lector.menu(opcions,true);
            switch (opcio){
                case 'A':
                    //Afegeix despesa
                    afegirDespesa(facturacio);
                    break;
                case 'M':
                    //Modifica despesa
                    modificarDespesa(facturacio);
                    break;
                case 'E':
                    //Elimina despesa
                    eliminarDespesa(facturacio);
                    break;
                case 'H':
                    //Mostra l'ajuda del menu
                    mostrar.mostraTexte("ajudaEdicioDespeses");
                    break;
                case 'T':
                    //Surt del menú despeses
                    sortir=true;
                    break;
                default:
                    break;
            }
        }
    }

    /**Guarda una despesa al pressupost.
     * 
     * @param facturacio conté el pressupost de la comunitat
     * @author victor
     */
    public void afegirDespesa(ArrayList<Despesa> facturacio){
        boolean hiHaDesp = true;
        //Mentre hi hagi despeses per introduir se n'aniran demanant.
        while (hiHaDesp){
            double importDesp = lector.llegeixImport();
            char tipusDesp = lector.llegeixTipusDespesa();
            String descripcioDesp = lector.llegeixDescripcio();
            
            //Creem la despesa nova 
            Despesa despesa = new Despesa(importDesp, tipusDesp, descripcioDesp);
            //Guardem la despesa al pressupost
            facturacio.add(despesa);
            //Mostra el pressupost nou
            System.out.println(pressupost.mostrarPressupost(facturacio));
            //Demana a l'usuari si hi ha més despeses a introduir.
            hiHaDesp = lector.hiHaMesDesp();
        }
    }
    
    /**Demana quina despesa es vol modificar i mostra un menu per escollir quin 
     * element de la despesa triada es vol modificar. Segons la resposta executa 
     * el metode corresponent.
     * 
     * @param facturacio Conté el pressupost de la comunitat.
     */
    public void modificarDespesa(ArrayList<Despesa> facturacio){
        //Guarda el número de despesa introduït per l'ususari.
        int numDespesa = seleccionarDespesa(facturacio);
        
        //Declaració de variables
        String[] menu = {"[I]mport",
                         "[P] Tipus",
                         "[D]escripció",
                         "[H] Ajuda",
                         "[T]ornar"};
        char[] opcions ={'I','P','D','H','T'};
        //Si el numero seleccionat és una selecció vàlida de despesa (numero positiu
        //o 0) l'elimina. Si no, continua l'execució.
        if (numDespesa >= 0){        
            //Es mostra el menú i es tracten respostes fins que es seleccioni sortir.
            boolean sortir = false;
            char opcio;
            while (!sortir){
                System.out.println("SELECCIO ELEMENT A MODIFICAR");
                System.out.println(UtilitatsString.generarString(30,"-"));
                System.out.println("menu principal > despeses > edició de despeses > element a editar");
                mostrar.mostraMenu(menu);
                opcio = lector.menu(opcions,true);
                switch (opcio){
                    case 'I':
                        //Afegeix despesa
                        pressupost.modificarImport(facturacio, numDespesa);
                        break;
                    case 'P':
                        //Modifica despesa
                        pressupost.modificarTipus(facturacio, numDespesa);
                        break;
                    case 'D':
                        //Elimina despesa
                        pressupost.modificarDescripcio(facturacio, numDespesa);
                        break;
                    case 'H':
                        //Mostra l'ajuda del menu
                        mostrar.mostraTexte("ajudaEditarElement");
                        break;
                    case 'T':
                        //Surt del menú despeses
                        sortir=true;
                        break;
                    default:
                        break;
                }
            }
        }
    }
    
    /**Mètode que elimina una despesa de l'ArrayList que conté el pressupost.
     * 
     * @param facturacio 
     */
    public void eliminarDespesa(ArrayList<Despesa> facturacio){
        //Guarda el número de despesa que l'usuari vol modificar 
        int numDespesa = seleccionarDespesa(facturacio);
        //Si el numero seleccionat és una selecció vàlida de despesa (numero positiu
        //o 0) l'elimina. Si no, continua l'execució.
        if (numDespesa >= 0){
            //Elimina la despesa triada
            facturacio.remove(numDespesa);
        }    
        //Mostra el pressupost modificat
        System.out.println(pressupost.mostrarPressupost(facturacio));
    }
    
    /**Mostra el pressupost i demana a l'usuari que introdueixi el numero corresponent
     * a la despesa que vol seleccionar.
     * 
     * @param facturacio Conté el pressupost
     * @return enter que selecciona l'usuari
     */ 
    public int seleccionarDespesa(ArrayList<Despesa> facturacio){
        boolean opcioOK = false;
        int numDespesa = 0;
        while (!opcioOK) {
            System.out.println(pressupost.mostrarPressupost(facturacio));
            System.out.println("Seleccioni el número de la despesa que vol modificar: ");
            System.out.println("Si selecciona '0' sortirà sense sleccionar cap despesa.");
            numDespesa = lector.llegirEnter();
            if ((numDespesa>=0)&&(numDespesa<=facturacio.size())) {
                opcioOK = true;
            } else {
                mostrar.mostraTexte("opcioInexistent");
            }
        }
        return numDespesa-1;
    }   
}
