 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.m3.uf2.interficie;

/** Inclou diverses utilitats que tracten dades del tipus String.
 *
 * @author oriol
 * @author victor
 * @author oscar
 */
public class UtilitatsString {
    
    /**Converteix un array de cadenes de text en una sola cadena, on cada
     * element de l'array s'escriu en una linia diferent i amb una seqüència d'inici.
     * Agafa l'array d'Strings i crea una sola cadena de text on cada element s'inicia
     * amb un caracter o seqüència de caracters configurable i cada element està 
     * separat ocupa una nova linia "\n".
     * Exemple:
     *       donat  l'array ={"Hola", "Com", "et", "dius", "?" };
     *           cadena = arrayAString (array, "- ");
     *       la cadena resultant serà:
     *           - Hola
     *           - Com
     *           - et
     *           - dius
     *           - ?
     *       es a dir cadena="- Hola\n- Com\n- et\n- dius\n- ?";
     * 
     * @param arrayCadenes Array que conte les cadenes de text
     * @param iniciLinia Seqüència a l'inici de cada element de l'array
     * @return cadena de text
     */
    public static String arrayAString(String[] arrayCadenes, String iniciLinia){
        String resultat = "";
        for (int i = 0; i < arrayCadenes.length; i++){
           resultat = resultat + iniciLinia + arrayCadenes[i] + "\n";
        }
        return resultat;
    }
    
    /** Genera una cadena de text d'un caràcter escollit per l'usuari 
     * i de la mida que trii l'usuari. Entrem com a paràmetres la mida i el
     * caràcter que volem que escrigui.
     * 
     * Exemple:
     * 
     * cadena = generarString(6,"*");
     * 
     * cadena = "******";
     * 
     * @param mida Enter que indica la mida que tindrà l'String.
     * @param caracterFarciment String que marca el caràcter que formarà la cadena
     * @return Cadena generada
     */
    public static String generarString(int mida, String caracterFarciment){
        String cadenaText ="";
        for (int i = 0; i < mida; i++){
            cadenaText = cadenaText + caracterFarciment;
        }
        return cadenaText;
    }
    
    /** Ajusta la mida dels Strings a la mida escollida. Aquest mètode només comprova
     * la mida de la cadena i escull si cal allargar o retallar la cadena. Utilitza
     * mètodes per realitzar les accions sobre la cadena (retallarCadena i ampliarCadena).
     * Si sabem segur quina acció volem que realitzi podem escollir els mètodes desitjats
     * directament.Si la cadena és més llarga que la mida desitjada retalla la cadena. 
     * Si la cadena original és més curta que la mida desitjada li afegim el caràcter
     * que volguem(l'entrem com a paràmetre)i escollim el costat ('E' per esquerra,
     * 'D' per dreta) on volem afegir els caràcters que calguin.
     *  
     * @param mida Mida de l'String
     * @param cadena Cadena que es vol ajustar
     * @param caracter Caracter per completar la cadena
     * @param costat Costat pel qual es vol completar la cadena (E esquerra, D dreta).
     * @return Cadena ajustada a la mida
     */
    public static String ajustarMidaString(int mida, String cadena, String caracter, char costat){
        if (cadena.length()<mida){
            cadena=ampliarCadena(cadena, mida, caracter, costat);
        } else if (cadena.length()>mida){
            cadena=retallarCadena(cadena, mida);
        }
        return cadena;
    }
    
    /** Afegeix caracters a la cadena fins aconseguir la mida desitjada. Aquest
     * mètode només escull segons el paràmetre d'entrada costat si cal executar
     * els mètodes afegirCaracterEsquerra o afegirCaracterDreta. Si volem afegir
     * els caràcters a un costat directament podem executar el mètode corresponent.
     * 
     * @param cadena Cadena que es vol ampliar
     * @param mida Mida que ha de tenir la cadena
     * @param caracter Caràcter que volem afegir
     * @param costat Costat de la cadena original on volem que s'afegeixi
     * @return Cadena de mida ajustada
     */
    public static String ampliarCadena(String cadena, int mida, String caracter, char costat){
        if (costat=='E'){
            cadena = afegirCaracterEsquerra(cadena, mida, caracter);
        } else if (costat=='D'){
            cadena = afegirCaracterDreta(cadena, mida, caracter);
        } else {
            // No hauria de passar. L'entrada hauria de ser E o D.
            System.out.println("Opcio no vàlida.");
        }
        return cadena;
    }
    
    /**Afegeix caràcters a l'esquerra de la cadena fins a completar la
     * mida desitjada.
     * Exemple: 
     *      String cadena = "cognom";
     *      String novaCadena = afegirCaracterEsquerra(cadena, 9, "*");
     *      novaCadena = "***cognom";
     * 
     * 
     * @param cadena Cadena a ampliar
     * @param mida Mida que es vol aconseguir
     * @param caracter Caràcter que s'afegeix
     * @return Cadena resultant
     */
    public static String afegirCaracterEsquerra(String cadena, int mida, String caracter){
        int numCaracters = mida-cadena.length();
        while (numCaracters>0){
            cadena=caracter+cadena;
            numCaracters--;
        }
        return cadena;
    }
    
    /**Afegeix caràcters a la dreta de la cadena fins a completar la
     * mida desitjada.
     *  Exemple: 
     *      String cadena = "cognom";
     *      String novaCadena = afegirCaracterDreta(cadena, 9, "*");
     *      novaCadena = "cognom***";
     * 
     * @param cadena Cadena a ampliar
     * @param mida Mida que es vol aconseguir
     * @param caracter Caràcter que s'afegeix
     * @return Cadena resultant
     */
    public static String afegirCaracterDreta(String cadena, int mida, String caracter){
        for (int i = cadena.length(); i<mida;i++){
            cadena = cadena + caracter;
        }
        return cadena;
    }
    
    /**Retalla una cadena fins la mida desitjada. Retorna la cadena de text de la
     * mida que volem acabada amb els caracters "...".
     * Exemple:
     *      String cadena = "Nom i cognoms de la persona";
     *      String novaCadena = retallarCadena(cadena, 15);
     *      novaCadena = "Nom i cognom...";
     * 
     * @param cadena Cadena que es vol retallar
     * @param mida Mida que volem que tingui la cadena
     * @return Cadena resultant
     */
    public static String retallarCadena(String cadena, int mida){
        cadena = cadena.substring(0,mida-3);
        cadena = cadena + "...";
        return cadena;
    }
}