/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.m3.uf2.interficie;

import ioc.eines.Scanner;
/**
 *
 * @author oriol
 * @author victor
 * @author oscar
 */
public class EntradaTeclat {
    
    SortidaPantalla mostrar = new SortidaPantalla();
    Scanner lector = new Scanner(System.in);
    
    /** Mètode que recull l'opció seleccionada en un menú, converteix l'string
     * recollit a majúscules i n'evalua la llargària i el valor en funció de
     * l'array d'opcions que se li passa com a paràmetre. En funció de la
     * variable error es mostren per pantalla els errors o no.
     * 
     * @param opcions Array de char amb les possibles opcions vàlides.
     * @param error Ens indica si es mostraran o no els missatges d'error.
     * @return Retorna el char corresponent a l'opció correcte si és el cas o x 
     * en la resta de casos.
     */
    public char menu(char[] opcions, boolean error) {
        String resposta = lector.nextLine().toUpperCase();
        if (resposta.length() == 1) {
            for (int i = 0; i < opcions.length; i++) {
                if (resposta.charAt(0) == opcions[i]) {
                    return opcions[i];
                }
            }
            //Com que ha fet tot el recorregut per les opcions i no l'ha trobat,
            //l'opció introduïda no era correcte.
            if (error) {
                mostrar.mostraTexte("opcioInexistent");
            }
        } else if (error) {
            //L'opció introduïda té masses caràcters.
            mostrar.mostraTexte("opcioLlarga");
        }
        //Si arriba a executar-se aquest codi és que no s'ha resolt correctament
        //la tria de l'opció. Passem char en minúscula, que segur que no és una
        //de les opcions vàlides.
        return 'x';
    }
    
    /** Mètode per seleccionar un pis de la comunitat a modificar.
     * 
     * @param opcions Array d'identificadors dels pisos de la comunitat.
     * @return identificador del pis a modificar
     */
    public String seleccionaPisModificar(String[] opcions) {
        String resultat = lector.nextLine().toUpperCase();
        System.out.println(resultat);
        if (resultat.length() == 2) {
            for (int i = 0; i < opcions.length; i++) {
                System.out.println(opcions[i]);
                if ((resultat.charAt(0) == opcions[i].charAt(0))&&(resultat.charAt(1) == opcions[i].charAt(1))) {
                    return opcions[i];
                }
            }
            //Si arriba a executar aquest codi és que l'opció no és vàlida però
            //té dos caràcters, o sigui és errònia.
            mostrar.mostraTexte("opcioInexistent");
        } else if (resultat.length() == 1) {
            //comprovem que l'opció entrada no sigui la que correspon a cancelar.
            if (resultat.charAt(0) == 'C') {
                return "C";
            }
            //Si no és el cas. l'opció torna a ser errònia.
            mostrar.mostraTexte("opcioInexistent");
        } else {
            //en els altres casos l'opció també és errònia.
            mostrar.mostraTexte("opcioInexistent");
        }
        //En cualsevol cas en les opcions errònies retornem una minúscula.
        return "x";
    }
    
    /** Mètode per llegir un string.
     * 
     * @return l'string llegit
     */
    public String llegeixString() {
        return lector.nextLine();
    }
    
    /** mètode per llegir un real
     * 
     * @return real llegit
     */
    public double llegeixReal() {
        double resultat = 0;
        boolean fet = false;
        while (!fet) {
            if(lector.hasNextDouble()) {
                resultat = lector.nextDouble();
                fet = true;
            } else {
                lector.next();
            }
        }
        lector.nextLine();
        return resultat;
    }
    
    /** Mètode per llegir un boleà
     * 
     * @return el boleà llegit
     */
    public boolean llegeixBool() {
        boolean resposta = true;
        boolean fet = false;
        while(!fet) {
            mostrar.mostraTexte("demanaBool");
            char[] opcions = {'S', 'N'};
            char tria = menu(opcions, true);
            switch (tria) {
                case 'N':
                    resposta = false;
                case 'S':
                    fet = true;
                    break;
            }
        }
        return resposta;
    }
    
    /**Pregunta a l'usuari si hi ha més despeses per introduir i retorna un boolea
     * segons la resposta que dóna l'usuari.
     * 
     * @return true si hi ha mes despeses per introduir o false si no n'hi ha.
     * @author victor
     */
    public boolean hiHaMesDesp(){   
        //Declaració variables
        boolean hiHaDesp = false;
        boolean llegit = false;
        System.out.println("Vol introduir més despeses?");
        
        //Mentre no s'introdueixi un valor vàlid s'executa el bucle
        while (!llegit){
            System.out.println("Introdueixi [S]i o [N]o i premi 'retorn'.");
            String resposta = lector.nextLine().toUpperCase();
            if (resposta.length() == 1) {//Comprova si la resposta té només un caràcter
                if (resposta.charAt(0) == 'S'){ //si la resposta és S
                    llegit = true;
                    hiHaDesp = true;
                } else if (resposta.charAt(0) == 'N'){ //Si la resposta es N
                    llegit = true;
                    hiHaDesp = false;
                } else {
                    //la opció escollida té la longitud correcta pero no és vàlida
                    mostrar.mostraTexte("opcioInexistent");
                }
            } else {
                //L'opció introduïda té masses caràcters.
                mostrar.mostraTexte("opcioLlarga");
            }
        }
        return hiHaDesp;
    }
    
    /** Mostra per pantalla el missatge del paràmetre i retorna el primer caracter  
     * entrat per l'usuari. En cas que l'usuri premi <code>ENTRAR</code> sense 
     * haver escrit cap caracter, s'informarà a l'usuari que s'està esperant una 
     * dada de tipus caràcter.
     *
     * @param missatge és el missatge a mostra per pantalla abans de demanar 
     * l'entrada.
     * @return char introduït per l'usuari.
     */
    public char entrarCharMaj(String missatge){
        String linia;
        char ret = 0;
        boolean correcte=false;
        do{
            System.out.println();
            System.out.print(missatge);
            linia = lector.nextLine();
            correcte=linia.length()==1;
            if(correcte){
                ret=linia.toUpperCase().charAt(0);
            }else if(linia.length()>1){
                System.out.print("Cal que introduïu un únic caràcter");
            }else{
                System.out.print("Cal que premeu una tecla de tipus caràcter i premeu [ENTRAR]");
            }
        }while(!correcte);   
        return ret;
    } 
    
    /**Llegeix un nombre enter entrat per teclat, comprova que sigui un nombre enter.
     * 
     * @return enter llegit
     */
    public int llegirEnter() {
        int resultat = 0;
        boolean fet = false;
        while (!fet) {
            if(lector.hasNextInt()) {
                resultat = lector.nextInt();
                fet = true;
            } else {
                lector.next();
                mostrar.mostraTexte("noEnter");
            }
        }
        lector.nextLine();
        return resultat;
    } 
    
    /**Demana a l'usuari que introdueixi l'import de la despesa i en retorna el valor
     * entrat.
     * 
     * @return import entrat per teclat. Sempre serà positiu.
     */
    public double llegeixImport(){
        double importDesp = 0;
        while (importDesp <= 0 ){
            System.out.println ("Introdueixi l'import de la despesa:");
            importDesp = llegeixReal();
            if (importDesp <= 0){
                mostrar.mostrarError("importNegatiu");
            }
        }
        return importDesp;
    }
    
    /**Demana a l'usuari que introdueixi per teclat el tipus de despesa que està introduint
     * i en retorna el tipus.
     * 
     * @return tipus de la despesa
     */
    public char llegeixTipusDespesa(){
        boolean tipusOk=false;
        char tipus;
        do{
        tipus = entrarCharMaj("Indica el tipus de despesa que acabes d'introduir "
                + "(A/B/C) i prem [ENTRAR]:");
            if(tipus=='A'||tipus=='B'||tipus=='C'){
                tipusOk=true;
            }else{
                mostrar.mostrarError("Els tipus de despesa només poden ser A, B o C");
            }
        }while(!tipusOk);
        return tipus;
    }
    
    /**Demana a l'usuari que introdueixi per teclat la descripció de la despesa 
     * que està introduint i en retorna la cadena de text.
     * 
     * @return Retorna la cadena de text amb la descricpió
     */
    public String llegeixDescripcio(){
        System.out.println("Indiqui la descripció de la despesa que acaba d'introduir"
                + " i premi 'retorn':");
        String descripcio = "";
        while (descripcio.equals("")){
            descripcio = llegeixString();
            if (descripcio.equals(""))
                mostrar.mostrarError("La descripció de la despesa"
                        + "no pot estar buida. Ha d'introduir algun text.");
        }
        return descripcio;
    }   
}