/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.m3.uf2.interficie;

import ioc.m3.uf2.models.Pis;
import java.util.ArrayList;

/**
 *
 * @author oriol
 * @author victor
 * @author oscar
 */
public class SortidaPantalla {
    
    static final String[][] TEXTES = {
        {"comunitatBuida", "\nNo hi ha pisos a la comunitat, primer hauries"
                + " d'importar les dades.\n"},
        {"demanaAbsent", "Selecciona si el propietari viu al pis o no."},
        {"demanaBool", "Entra [S]i o [N]o."},
        {"demanaCoef", "Entra un nou coeficient per al pis."},
        {"demanaExempt", "Selecciona si el propietari està exempt de pagar "
                + "les despese de tipus C."},
        {"demanaIndexMostrarCom", "Selecciona com vols visualitzar la " 
                + "comunitat: per ordre [A]lfabètic, ordenats per [C]oeficients"
                + "i qualsevol altra opció els mostrarà en ordre natural."},
        {"demanaNom", "Entra un nou nom pel propietari del pis."},
        {"demanaTel", "Entra un nou telèfon per al pis."},
        {"opcioLlarga", "L'opció que has introduït té masses caràcters. "
                + "Torna-hi si us plau."},
        {"opcioInexistent", "L'opció que has introduït no existeix. Torna-hi "
                + "si us plau."},
        {"presentacioPrograma", "Aquest programa..."},
        {"presentacioPropietaris", "En aquesta secció podràs importar, "
                + "modificar i visualitzar els propietaris de la comunitat."},
        {"seleccionaDadaModificar", "Selecciona la dada a modificar entrant "
                + "el número que la identifica, o [C]ancela."},
        {"seleccionaPis", "Selecciona el pis a modificar. Entra els dos "
                + "caràcters que identifiquen el pis escollit o [C]ancela." },
        {"ajudaDespeses", "AJUDA DEL MENU DESPESES\n"
                + "-----------------------\n"
                + "Seleccioni l'opció desitjada mitjançant la lletra entre claus\n"
                + "i premi la tecla 'retorn'.\n"
                + "Explicació de les opcions:\n"
                + "MOSTRAR PRESSUPOST: mostra el llistat de les despeses introduïdes\n"
                + "amb l'import, el tipus de despesa i la descripció guardats.\n"
                + "EDITAR PRESSUPOST: permet afegir despeses, eliminar-ne o modificar-ne\n"
                + "el contingut.\n"
                + "CALCULAR IMPORTS DE CADA PIS: mostra un llistat dels pisos amb\n"
                + "l'import que pertoca pagar a cada pis.\n"
                + "AJUDA: mostra aquesta ajuda.\n"
                + "TORNAR: surt del menú despeses i torna al menú anterior.\n"},
        {"ajudaEdicioDespeses","AJUDA DEL MENU D'EDICIÓ DE DESPESES\n"
                + "-------------------------------------\n" 
                + "Seleccioni l'opció desitjada mitjançant la lletra entre claus\n"
                + "i premi la tecla 'retorn'.\n"
                + "Explicació de les opcions:\n"
                + "AFEGIR DESPESA: serveix per introduir una nova despesa al \n"
                + "pressupost amb l'import, el tipus de despesa i la descripció.\n"
                + "MODIFICAR DESPESA: permet modificar els detalls d'una despesa \n"
                + "com l'import, el tipus o la descripció.\n" 
                + "ELIMINAR DESPESA: serveix per eliminar una de les despeses que \n"
                + "seleccioni l'usuari.\n"
                + "AJUDA: mostra aquesta ajuda.\n"
                + "TORNAR: surt del menú d'edició de despeses i torna al menú \n"
                + " anterior.\n"},
        {"ajudaEditarElement","AJUDA DE SELECCIO D'ELEMENT A EDITAR\n"
                + "-------------------------------------\n" 
                + "Seleccioni l'opció desitjada mitjançant la lletra entre claus\n"
                + "i premi la tecla 'retorn'.\n"
                + "Explicació de les opcions:\n"
                + "IMPORT DE LA DESPESA: serveix per introduir un nou import a la despesa. \n"
                + "TIPUS DE LA DESPESA: permet modificar el tipus de la despesa. \n" 
                + "DESCRIPCIÓ DE LA DESPESA: serveix per canviar la descripció de la despesa\n"
                + "AJUDA: mostra aquesta ajuda.\n"
                + "TORNAR: surt del menú d'edició de despeses i torna al menú \n"
                + " anterior.\n"},
        {"noDespeses","Encara no s'ha introduït cap despesa al pressupost."},
        {"importNegatiu","L'import de la despesa ha de ser més gran que 0."},
        {"noEnter","Ha d'introduir un nombre enter."}
    };
    
    /** Mètode que mostra un texte (la presentació d'una secció, d'una 
     * pàgina,...). Se li ha de passar un índex, de manera que els textes 
     * estiguin en un array d'(n x 2), on busquem la clau a la columna 0.
     * 
     * @param clau String clau que identifica el texte a mostrar.
     */
    public void mostraTexte(String clau) {
        boolean trobat = false;
        for (int i = 0; i < TEXTES.length; i++) {
            if (clau.equalsIgnoreCase(TEXTES[i][0])) {
                System.out.println(TEXTES[i][1]);
                trobat = true;
            }
        }
        if (!trobat) {
            System.out.println("ERROR: No s'ha trobat el texte referenciat.");
        }
    }
    
     /**Mostra en pantalla el menú les opcions del qual estan guardats en un array
     * de cadenes de text. Cada element de l'array conté una de les opcions. 
     * Exemple: 
     *      String[] opcions = {"[L]loc de la reunió", "[D]ia de la reunió",
     *                          "[M]ètode de la reunió"};
     *      mostrarMenu(String[] opcions);
     * Aixo mostra per pantalla:
     *      MENU DE SELECCIO
     *      --------------------
     *           -[L]loc de la reunió
     *           -[D]ia de la reunió
     *           -[M]ètode de la reunió
     *      Introdueixi l'opció escollida: 
     * 
     * @param opcions String[] que conté les opcions del menú
     */
    public void mostraMenu(String[] opcions){
        for (int i = 0;i < opcions.length;i++){
            System.out.println("\t-" + opcions[i]);//Mostra les opcions
        }
        System.out.print("Introdueixi l'opció escollida: ");
    }
    
    // Pantalles de la secció PROPIETARIS
    
    /** Mètode que ens mostra els propietaris dels pisos, ordenats per un dels
     * índexs. Se li ha de passar l'array de propietaris, l'array indexs i
     * l'index a usar.
     * 
     * @param string String a mostrar 
     */
    public void mostraString(String string) {
        System.out.println(string);
    }
    
    /**Mostra l'import de despeses que ha de pagar cada pis
     * 
     * @param comunitat ArrayList que conté les dades de la comunitat
     * @param deutesPisos Conté l'import de despeses que ha de pagar cada pis
     */
    public void mostrarDeutesPerPisos(ArrayList<Pis> comunitat, double[] deutesPisos){
        System.out.println("Relació de les despeses detallades per propietari: ");
        for (int i = 0; i<deutesPisos.length; i++) {
            Pis pis = comunitat.get(i);
            System.out.println("- " + pis.nom + ": " + deutesPisos[i]);
        }   
    }
    
    /** Mostra la junta. Se li ha de passar l'array de la junta.
     * 
     * @param junta 
     */
    public void mostraJunta(String[] junta) {
        String texte = "\nMembres de la junta:\n"
                + "     President - " + junta[0] + "\n"
                + "     Vicepresident - " + junta[1] + "\n"
                + "     Secretari - " + junta[2] + "\n";
        mostraString(texte);
    }
    
    /**Afegeix una seqüència de caràcters ("***ERROR***") al principi d'un missatge
     * d'error per tal de fer-lo més visible.
     * 
     * Exemple: Si un nombre entrat no és un enter quan ho ha de ser.
     *        mostrarError("El valor entrat no es un enter");
     *        "***ERROR*** El valor entrat no es un enter"
     * 
     * @param missatge String missatge d'error que es vol mostrar
     */
    public void mostrarError(String missatge){
        System.out.println("***ERROR***"+missatge);
    }
    
    /**Mostra el contingut d'un array unidimensional que conté dades del tipus String.
     * 
     * @param array Array que conté les dades que es vol mostrar.
     */
    public void mostrarArray(String[] array){
        for (int i = 0; i<array.length; i++){
            if (array[i].equals("")){    //Si l'String és buit en la posició ho indica.
                System.out.println("[Element buit]");
            } else {
                System.out.println("\t"+array[i]);
            }
        }
    }
    
    /**Mostra el contingut d'un array bidimensional que conté dades del tipus String.
     * 
     * @param arrayBidi Array que conté les dades que es vol mostrar.
     */
    public void mostrarArrayBidi(String[][] arrayBidi){
        for (int i = 0; i<arrayBidi.length; i++) {
            for (int j = 0; j<arrayBidi[i].length;j++){
                if (arrayBidi[i][j].equals("")){//Si l'String és buit en la posició ho indica.
                    System.out.print("\t[Element buit]");
                } else {
                    System.out.print("\t"+arrayBidi[i][j]);
                } 
            }
            System.out.println();
        }
    }
}
